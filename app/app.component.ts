import {Component} from 'angular2/core';
import {ChildComponent} from './child.component';
import {SharedDataService} from './sharedData.service';

@Component({
    selector: 'my-app',
    template: `<h1>Component iteraction demo (Sharing data via Service) </h1>
                <div>Entered text: {{dataService.sharedData}}</div> 
                <childComponent></childComponent>
              `,
    directives: [ChildComponent],
    providers: [SharedDataService]
})
export class AppComponent { 
    constructor(public dataService: SharedDataService) {
    }
}