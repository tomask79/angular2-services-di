System.register(['angular2/core', './sharedData.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, sharedData_service_1;
    var ChildComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (sharedData_service_1_1) {
                sharedData_service_1 = sharedData_service_1_1;
            }],
        execute: function() {
            ChildComponent = (function () {
                function ChildComponent(dataService) {
                    this.dataService = dataService;
                }
                ChildComponent = __decorate([
                    core_1.Component({
                        selector: 'childComponent',
                        template: '<input type=text [(ngModel)]="dataService.sharedData" />'
                    }), 
                    __metadata('design:paramtypes', [sharedData_service_1.SharedDataService])
                ], ChildComponent);
                return ChildComponent;
            }());
            exports_1("ChildComponent", ChildComponent);
        }
    }
});
//# sourceMappingURL=child.component.js.map