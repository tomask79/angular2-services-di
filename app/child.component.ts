import {Component} from 'angular2/core';
import {SharedDataService} from './sharedData.service';

@Component({
    selector: 'childComponent',
    template: '<input type=text [(ngModel)]="dataService.sharedData" />'
})

export class ChildComponent {
    constructor(public dataService: SharedDataService) {
    }
}