# Writing applications in Angular 2 [part2] #

In my previous post regarding [Angular 2](https://angular.io/) I was testing the creation of [Angular 2 components](https://bitbucket.org/tomask79/angular2-components/overview).
Well, we could be fine with just components in Angular 2, but we need something where to put for example logic of getting the data from server or reusable business code we could use across our components. For this purpose Angular 2 offers [services](https://angular.io/docs/ts/latest/tutorial/toh-pt4.html).

## Angular 2 Services and Dependency Injection ##

Okay, first of all, *Angular 2 Service is a class with focused purpose*. You can use Angular 2 Services for:

* External systems iteractions.
* Creation of reusable code across components..
* Or for *sharing the data between components*.

But how do we create and use Angular 2 Services? 

* We need to create a class
* Add a metadata to it
* And register our service into components where we want to use it via Angular 2 Provider. 

Okay, let's begin. Let's test the feature regarding of **sharing the data between components via Angular 2 Service** 
which is going to be defined like this (**sharedData.service.ts**):

```
import {Injectable} from 'angular2/core';

@Injectable()
export class SharedDataService {
    public sharedData: string = "Initial Value";
}
```
* **sharedData** is a field which will containt the DATA shared between components.
* **Injectable** is an optional decorator and it's an recommended way to mark services with it (helper for Angular2 built-in Injector)

Now let's **INJECT this service into two components** which will be **sharing the SharedDataService.sharedData field**. 

**First, the root component**:
(We will construct parent-child component app where child component will change the shared data and parent will display it, just for test of DI)

```
import {Component} from 'angular2/core';
import {ChildComponent} from './child.component';
import {SharedDataService} from './sharedData.service';

@Component({
    selector: 'my-app',
    template: `<h1>Component iteraction demo (Sharing data via Service) </h1>
                <div>Entered text: {{dataService.sharedData}}</div> 
                <childComponent></childComponent>
              `,
    directives: [ChildComponent],
    providers: [SharedDataService]
})
export class AppComponent { 
    constructor(public dataService: SharedDataService) {
    }
}
```

Let's focus on the constructor. By the code "**constructor(public dataService: SharedDataService)**" i'm telling: 
"Hey Angular2, I need an instance of SharedDataService service inside of AppComponent ". Typescript syntax "public dataService: SharedDataService"
will also add dataService as a field of AppComponent class. Process of Injecting services into components like this is called "DEPENDENCY INJECTION".

**Now maybe the most important part! To actually use our service in our component we need to register a provider. To register a provider we need to define it as a part of component metadata like we did in our AppComponent...providers: [SharedDataService]. Now! By registering a provider like this, our SharedDataService is injectable into our AppComponent and ANY OF IT'S CHILDREN! And Angular 2 Injector will provide always the same instance, a SINGLETON! And yes, by registering new provider of a service, we will get completely new instance of our service, injected by Angular 2 Injector. So take Provider as a layer telling the Angular 2 Injector(manages registered instances) which instance of service to create or return during dependency injection.**  

** Now, the child component **

```
import {Component} from 'angular2/core';
import {SharedDataService} from './sharedData.service';

@Component({
    selector: 'childComponent',
    template: '<input type=text [(ngModel)]="dataService.sharedData" />'
})

export class ChildComponent {
    constructor(public dataService: SharedDataService) {
    }
}
```
As you can see, we injected our SharedDataService here again. Now, ChildComponent is a **child component of our AppComponent** which means that **Angular 2 Dependency Injection process will provide the same instance of SharedDataService as in AppComponent, a SINGLETON**. So new value entered in the input in child component thanks to two-way databinding **[(ngModel)]="dataService.sharedData"** (I will explain in another parts) **is going to be visible immediatelly in our master AppComponent**. Without any event emitting or other techniques of component interaction. 

It's really crucial to understand how dependency injection works in Angular 2. This demo isn't maybe typical way of how you're going to be building your applications (child component usually emits event to parent when it's work is done), but I just wanted to test Services dependency injection process and whether child components will really get SINGLETON of injected service...And yes, **sharing data via service** like this really works, as Angular 2 guides say...

### Testing the demo ###

* git clone <URL of this repo>
* npm install (in the angular2-services-di directory)
* npm start
* visit http://localhost:3000

Hope you found this usefull. 

regards

Tomas